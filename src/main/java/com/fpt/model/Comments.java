package com.fpt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "comments")
public class Comments {
	
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private Integer id;

		@Column

		private int post_id;
		@Column

		private int user_id;
		public Comments() {
			super();
		}
		public Comments(int post_id, int user_id) {
			super();
			this.post_id = post_id;
			this.user_id = user_id;
		}

		

}
