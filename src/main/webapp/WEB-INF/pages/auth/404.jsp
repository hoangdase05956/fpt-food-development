<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html><head>
<title>Oops 404 for Website Template for free | Home :: w3layouts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="//fonts.googleapis.com/css?family=Love+Ya+Like+A+Sister" rel="stylesheet" type="text/css">
<style type="text/css">
body{
	font-family: 'Love Ya Like A Sister', cursive;
}
body{
	background:#eaeaea;
}	
.wrap{
	margin:0 auto;
	width:1000px;
}
.logo{
	text-align:center;
	margin-top:200px;
}
.logo img{
	width:350px;
}
.logo p{
	color:#272727;
	font-size:40px;
	margin-top:1px;
}	
.logo p span{
	color:lightgreen;
}	
.sub a{
	color:#fff;
	background:#272727;
	text-decoration:none;
	padding:10px 20px;
	font-size:13px;
	font-family: arial, serif;
	font-weight:bold;
	-webkit-border-radius:.5em;
	-moz-border-radius:.5em;
	-border-radius:.5em;
}	
.footer{
	color:black;
	position:absolute;
	right:10px;
	bottom:10px;
}	
.footer a{
	color:rgb(114, 173, 38);
}	
</style>
<div id="_bsa_srv-CKYI653J_0"></div><div id="_bsa_srv-CKYI627U_1"></div><link id="codefund-style" rel="stylesheet" media="all" href="https://codefund.io/packs/css/code_fund_ad-e33b44c1.css"><script id="codefund-script" type="text/javascript" src="https://codefund.io/packs/js/code_fund_ad-c5122a9eb2cf2a34fc49.js"></script></head>


<body style="">
<script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script><script src="//m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
<script>
(function(){
	if(typeof _bsa !== 'undefined' && _bsa) {
  		// format, zoneKey, segment:value, options
  		_bsa.init('flexbar', 'CKYI627U', 'placement:w3layoutscom');
  	}
})();
</script>
<script>
(function(){
if(typeof _bsa !== 'undefined' && _bsa) {
	// format, zoneKey, segment:value, options
	_bsa.init('fancybar', 'CKYDL2JN', 'placement:demo');
}
})();
</script>
<script>
(function(){
	if(typeof _bsa !== 'undefined' && _bsa) {
  		// format, zoneKey, segment:value, options
  		_bsa.init('stickybox', 'CKYI653J', 'placement:w3layoutscom');
  	}
})();
</script>
<!--<script>(function(v,d,o,ai){ai=d.createElement("script");ai.defer=true;ai.async=true;ai.src=v.location.protocol+o;d.head.appendChild(ai);})(window, document, "//a.vdo.ai/core/w3layouts_V2/vdo.ai.js?vdo=34");</script>-->
<div id="codefund"><!-- fallback content --></div>
<script src="https://codefund.io/properties/441/funder.js" async="async"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-149859901-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-149859901-1');
</script>

<script>
     window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
     ga('create', 'UA-149859901-1', 'demo.w3layouts.com');
     ga('require', 'eventTracker');
     ga('require', 'outboundLinkTracker');
     ga('require', 'urlChangeTracker');
     ga('send', 'pageview');
   </script>
<script async="" src="/js/autotrack.js"></script>

<meta name="robots" content="noindex">
<link rel="stylesheet" href="/images/demobar_w3_4thDec2019.css">
	<!-- Demo bar start -->
  <div id="w3lDemoBar" class="w3l-demo-bar">
  <a href="https://w3layouts.com/?p=" ga-on="click" ga-event-category=" Template" ga-event-action="view" ga-event-label=" - view">
    <span class="w3l-icon -back">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path opacity=".87" fill="none" d="M0 0h24v24H0V0z"></path><path d="M16.62 2.99c-.49-.49-1.28-.49-1.77 0L6.54 11.3c-.39.39-.39 1.02 0 1.41l8.31 8.31c.49.49 1.28.49 1.77 0s.49-1.28 0-1.77L9.38 12l7.25-7.25c.48-.48.48-1.28-.01-1.76z"></path></svg>
    </span>
    <span class="w3l-text">Back</span>
  </a>
  <a href="https://w3layouts.com/" class="w3l-logo">W3layouts</a>
  
</div>


<!---728x90--->

 <div class="wrap">
	<div class="logo">
			<p>OOPS! - Could not Find it</p>
			<img src="images/404-1.png">
			
<!---728x90--->

			<div class="sub">
			  <p><a href="#">Back </a></p>
			</div>
	</div>
 </div>	
	
<!---728x90--->

	
	<div class="footer">
	 � 2012 Oops 404 . All Rights Reserved | Design by<a href="http://w3layouts.com">W3layouts</a>
	</div>
	
</body></html>